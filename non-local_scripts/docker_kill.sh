source ../container-name.sh

IMAGE_NAME=$1


TAG="latest"

if [ $# -lt 1 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 <docker image>"
    echo "+ $0 reliableembeddedsystems/${CONTAINER_NAME}"
    echo "+ $0 reliableembeddedsystems/${CONTAINER_NAME}:${TAG}"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

set -x
docker ps -a
docker stop ${ID_TO_KILL}
docker rm -f ${ID_TO_KILL}
docker ps -a
set +x

