#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="$(basename $DIR)"
ARCH="-x86-64"
CONTAINER_NAME="${DIR}${ARCH}"
TAG="latest${ARCH}"
IMAGE_NAME="app-container-image-telegraf-prebuilt-oci${ARCH}"
